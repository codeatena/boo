//
//  CommentTableViewCell.m
//  BOO
//
//  Created by AnCheng on 8/14/15.
//  Copyright (c) 2015 ancheng1114. All rights reserved.
//

#import "CommentTableViewCell.h"
#import "NSDate+PrettyTimestamp.h"

@implementation CommentTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setData:(NSDictionary *)dic
{
    NSDate *date = [NewDateFormatter() dateFromString:dic[@"created_time"]];
    _dateLbl.text = [date prettyTimestampSinceNow];
    
    _nameLbl.text = [dic[@"from"] objectForKey:@"name"];
    _commentLbl.text = dic[@"message"];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?redirect=true&type=small" ,[dic[@"from"] objectForKey:@"id"]]] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize){
        
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL){
        
        _profileImageView.image = image;
    }];
    
}

static inline NSDateFormatter *NewDateFormatter() {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZ"];
    //Optionally for time zone conversions
    //[formatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    return formatter;
}

@end
