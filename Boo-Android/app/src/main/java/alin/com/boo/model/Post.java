package alin.com.boo.model;

import org.json.JSONObject;

/**
 * Created by User on 8/20/2015.
 */
public class Post {

    public String id = "";
    public String picture = "";
    public String video = "";
    public String message = "";
    public String created_time = "";
    public String type = "";
    public int comments_count = 0;
    public int likes_count = 0;

    public Post(JSONObject jsonObject) {
        try {
            id = jsonObject.getString("id");
            if (jsonObject.has("picture")) {
                if (!jsonObject.isNull("picture")) {
                    picture = jsonObject.getString("picture");
                }
            }
            if (jsonObject.has("video")) {
                if (!jsonObject.isNull("video")) {
                    video = jsonObject.getString("video");
                }
            }
            message = jsonObject.getString("message");
            created_time = jsonObject.getString("created_time");
            type = jsonObject.getString("type");
            comments_count = jsonObject.getJSONObject("comments").getJSONObject("summary").getInt("total_count");
            likes_count = jsonObject.getJSONObject("likes").getJSONObject("summary").getInt("total_count");
        } catch (Exception e) {

        }
    }
}