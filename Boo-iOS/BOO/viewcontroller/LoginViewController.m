//
//  LoginViewController.m
//  BOO
//
//  Created by AnCheng on 8/3/15.
//  Copyright (c) 2015 ancheng1114. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)doLogin:(id)sender
{
    if ([FBSDKAccessToken currentAccessToken])
    {
        [self performSegueWithIdentifier:@"feedSegue" sender:nil];
    }
    else
    {
        FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
        [login logInWithReadPermissions:nil handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
            
            if (error) {
                // Process error
                
                UIAlertView *av = [[UIAlertView alloc] initWithTitle:@"Error" message:error.localizedDescription delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [av show];
                
            } else if (result.isCancelled) {
                // Handle cancellations
            } else
            {
                [self performSegueWithIdentifier:@"feedSegue" sender:nil];
            }
            
        }];
    }
}

@end
