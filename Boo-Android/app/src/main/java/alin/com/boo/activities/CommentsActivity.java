package alin.com.boo.activities;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.paging.listview.PagingListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import alin.com.boo.Consts;
import alin.com.boo.R;
import alin.com.boo.model.Comment;
import alin.com.boo.model.Post;
import alin.com.boo.widget.listView.MyListAdapter;
import alin.com.boo.widget.listView.MyListItem;

public class CommentsActivity extends ActionBarActivity {

    final String TAG = "CommentsActivity";

    public static Post post;

    PagingListView listView;

    ArrayList<MyListItem> mItems;
    MyListAdapter myListAdapter;
    boolean isFirstPage;

    String nextPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);

        initWidget();
        initValue();
        initEvent();
    }

    private void initWidget() {
        listView = (PagingListView) findViewById(R.id.listView);
        addHeaderView();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initValue() {
        isFirstPage = true;

        mItems = new ArrayList<MyListItem>();
        myListAdapter = new MyListAdapter(this, mItems);
        listView.setAdapter(myListAdapter);

        listView.setHasMoreItems(true);
        listView.setPagingableListener(new PagingListView.Pagingable() {
            @Override
            public void onLoadMoreItems() {
                Log.e(TAG, "onLoadMoreItems");
                if (isFirstPage) {
                    loadComments();
                } else {
                    loadCommentsFromNext();
                }
            }
        });
    }

    private void initEvent() {

    }

    private void loadComments() {
        Log.e(TAG, AccessToken.getCurrentAccessToken().getToken());
        String url = post.id + "/comments";
        Bundle params = new Bundle();
        params.putString("fields", "feed.limit(10).fields(type,message,created_time,picture,source,comments.summary(true),likes.summary(true))");

        GraphRequest request = GraphRequest.newGraphPathRequest(AccessToken.getCurrentAccessToken(), url, new GraphRequest.Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {
                try {
                    JSONObject response =  graphResponse.getJSONObject();
                    Log.e(TAG, response.toString());
                    JSONArray data = response.getJSONArray("data");

                    for (int i = 0; i < data.length(); i++) {
                        JSONObject jsonComment = data.getJSONObject(i);
                        Comment comment = new Comment(jsonComment);
                        MyListItem item = new MyListItem(R.layout.row_comment, comment);
                        mItems.add(item);
                    }

                    JSONObject paging = response.getJSONObject("paging");
                    nextPage = paging.getString("next");

                    listView.onFinishLoading(true, null);
                    myListAdapter.notifyDataSetChanged();

                    isFirstPage = false;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        request.executeAsync();
    }

    private void loadCommentsFromNext() {
        Log.e(TAG, "next page: " + nextPage);

        Ion.with(this)
                .load(nextPage)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        try {
                            JSONObject response = new JSONObject(result.toString());

                            Log.e(TAG, response.toString());
                            JSONArray data = response.getJSONArray("data");

                            for (int i = 0; i < data.length(); i++) {
                                JSONObject jsonComment = data.getJSONObject(i);
                                Comment comment = new Comment(jsonComment);
                                MyListItem item = new MyListItem(R.layout.row_comment, comment);
                                mItems.add(item);
                            }

                            JSONObject paging = response.getJSONObject("paging");
                            nextPage = paging.getString("next");

                            listView.onFinishLoading(true, null);
                            myListAdapter.notifyDataSetChanged();

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                });
    }

    private void addHeaderView() {
        View view = View.inflate(this, R.layout.row_post, null);

        ImageView imgUserPhoto = (ImageView) view.findViewById(R.id.post_user_photo_imageView);
        TextView txtPostUserName = (TextView) view.findViewById(R.id.post_username_textView);
        TextView txtCreatedTime = (TextView) view.findViewById(R.id.post_create_time_textView);
        TextView txtPostMessage = (TextView) view.findViewById(R.id.post_message_textView);
        ImageView imgPostPhoto = (ImageView) view.findViewById(R.id.post_photo_imageView);
        TextView txtCommentsLikesCount = (TextView) view.findViewById(R.id.post_comments_likes_count_textView);

        txtPostUserName.setText(Consts.BOO_NAME);
        txtCreatedTime.setText(post.created_time);
        txtPostMessage.setText(post.message);
        txtCommentsLikesCount.setText(post.likes_count + " Likes " + post.comments_count + " Comments");
        Ion.with(imgPostPhoto).load(post.picture);
        Ion.with(imgUserPhoto).load(Consts.BOO_PROFILE_URL);

        listView.addHeaderView(view);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}