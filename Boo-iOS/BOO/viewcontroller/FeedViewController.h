//
//  FeedViewController.h
//  BOO
//
//  Created by AnCheng on 8/3/15.
//  Copyright (c) 2015 ancheng1114. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FeedPhotoTableViewCell.h"

@interface FeedViewController : UIViewController <UITableViewDataSource ,UITableViewDelegate ,FeedPhotoTableViewCellDelegate>

@property (nonatomic ,assign) IBOutlet UIImageView *profileImageView;
@property (nonatomic ,assign) IBOutlet UIImageView *coverImageView;

@property (nonatomic ,assign) IBOutlet UITableView *feedTableView;
@property (nonatomic ,strong) NSMutableArray *feedArr;
@property (nonatomic ,strong) NSString *pageUrl;

@end
