//
//  FeedViewController.m
//  BOO
//
//  Created by AnCheng on 8/3/15.
//  Copyright (c) 2015 ancheng1114. All rights reserved.
//

#import "FeedViewController.h"
#import "FeedPhotoTableViewCell.h"
#import "UIScrollView+DXRefresh.h"
#import "UIColor+HexString.h"
#import <MediaPlayer/MediaPlayer.h>
#import "CommentViewController.h"

@interface FeedViewController ()

@end

@implementation FeedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:[NSURL URLWithString:BOO_COVER_URL] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize){
        
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL){
        
        _coverImageView.image = image;
    }];
    
    [manager downloadImageWithURL:[NSURL URLWithString:BOO_PROFILE_URL] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize){
        
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL){
        
        _profileImageView.image = image;
    }];
    
    _feedTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [_feedTableView addHeaderWithTarget:self action:@selector(refreshHeader) withIndicatorColor:[UIColor colorWithHexString:@"#48A0DC"]];
    [_feedTableView addFooterWithTarget:self action:@selector(refreshFooter) withIndicatorColor:[UIColor colorWithHexString:@"#48A0DC"]];
    
    [self getFeed];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    CommentViewController *vc = (CommentViewController *)[segue destinationViewController];
    vc.postDic = sender;
}

- (void)getFeed
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    // comment url link
    //@"/225719294132332_866847843352804/comments"
    
    // post url link
    //@"/Boo?fields=feed.limit(100){type,message,created_time}

    //profile picture url
    //@"https://graph.facebook.com/user_id/picture?redirect=false&type=normal&width=110&height=110"
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"/Boo?fields=feed.limit(10).fields(type,message,created_time,picture,source,comments.summary(true),likes.summary(true))" parameters:nil]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         
         [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
         
         if (!error) {
             NSLog(@"fetched feed:%@", result);
             
             _feedArr = [[NSMutableArray alloc] initWithArray:[result[@"feed"]  objectForKey:@"data"]];
             _pageUrl = [[result[@"feed"] objectForKey:@"paging"] objectForKey:@"next"];
             [_feedTableView reloadData];
         }
         else
             NSLog(@"error : %@" ,error.localizedDescription);
     }];
    
}

#pragma mark - UITableViewDataSource ,UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _feedArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FeedPhotoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"feedCell" forIndexPath:indexPath];
    cell.celldelegate = self;
    NSDictionary *dic = [_feedArr objectAtIndex:indexPath.row];
    [cell setData:dic];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dic = [_feedArr objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"commentSegue" sender:dic];
}

- (void)playVideo:(FeedPhotoTableViewCell *)cell
{
    NSIndexPath *indexPath = [_feedTableView indexPathForCell:cell];
    NSDictionary *dic = [_feedArr objectAtIndex:indexPath.row];
    
    NSURL *videoURL = [NSURL URLWithString:dic[@"source"]];
    MPMoviePlayerViewController* moviePlayer=[[MPMoviePlayerViewController alloc] initWithContentURL:videoURL];
    [self presentMoviePlayerViewControllerAnimated:moviePlayer];
}

- (void)refreshHeader
{
    if ([_feedTableView isFooterRefreshing]) {
        
        [_feedTableView headerEndRefreshing];
        return;
    }
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:@"/Boo?fields=feed.limit(20).fields(type,message,created_time,picture,source)" parameters:nil]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         
         [_feedTableView headerEndRefreshing];
         if (!error) {
             NSLog(@"fetched feed:%@", result);
             
             _feedArr = [[NSMutableArray alloc] initWithArray:[result[@"feed"]  objectForKey:@"data"]];
             _pageUrl =  [[result[@"feed"] objectForKey:@"paging"] objectForKey:@"next"];
             [_feedTableView reloadData];
         }
         else
             NSLog(@"error : %@" ,error.localizedDescription);
     }];
}

- (void)refreshFooter
{
    if ([_feedTableView isHeaderRefreshing]) {
        
        [_feedTableView footerEndRefreshing];
        return;
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:_pageUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [_feedArr addObjectsFromArray:responseObject[@"data"]];
        _pageUrl =  [responseObject[@"paging"] objectForKey:@"next"];
        [_feedTableView reloadData];
        
        [_feedTableView footerEndRefreshing];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        

    }];
}

@end
