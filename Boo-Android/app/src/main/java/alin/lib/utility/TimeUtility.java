package alin.lib.utility;

import android.annotation.SuppressLint;
import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class TimeUtility {

	public static TimeUtility instance;

	public static TimeUtility getInstance() {
		if (instance == null) {
			instance = new TimeUtility();
		}
		return instance;
	}

	@SuppressLint("SimpleDateFormat")
	public String getCurrentTime() {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String date = df.format(Calendar.getInstance().getTime());
		
		return date;
	}
	
	@SuppressLint("SimpleDateFormat")
	public String getStringFromDate(Date date, String strFormat) {
		DateFormat df = new SimpleDateFormat(strFormat);
		String strDate = df.format(date);
		return strDate;
	}
	
	@SuppressLint("SimpleDateFormat")
	public Date getDateFromTimeStamp(long timeStamp) {
		Date date = new Date();
		date.setTime(timeStamp);
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String strDate = df.format(date);
		Log.e("strDate", strDate);
		
		return date;
	}
	
	public String getStringFromTimeStamp(long timeStamp, String strFormat) {
		Date date = new Date();
		date.setTime(timeStamp);
		return getStringFromDate(date, strFormat);
	}
	
	public Date getDateFromJsonString(String jsonStr) {

//		Log.e("JsonString", jsonStr);
		
		int k1 = jsonStr.indexOf("(") + 1;
		int k2 = jsonStr.indexOf(")");
		
//		Log.e("k1, k2 position", Integer.toString(k1) + ", " + Integer.toString(k2));
		String strTimeStamp = jsonStr.substring(k1, k2);
//		Log.e("strTimeStamp", strTimeStamp);
		
		return getDateFromTimeStamp(Long.parseLong(strTimeStamp));
	}
	
	public long getCurrentTimeStamp() {
		return Calendar.getInstance().getTimeInMillis();
	}
	
	@SuppressLint("SimpleDateFormat")
	public String getCurrentTimeAsFormat(String strFormat) {
		DateFormat df = new SimpleDateFormat(strFormat);
		String time = df.format(Calendar.getInstance().getTime());
		
		return time;
	}
	
	@SuppressLint("SimpleDateFormat")
	public Date getDateFromString(String strDate, String strFormat) {
		try{
			SimpleDateFormat sdf = new SimpleDateFormat(strFormat); // here set the pattern as you date in string was containing like date/month/year
			Date d = sdf.parse(strDate);
			return d;
		} catch(Exception ex){
			    // handle parsing exception if date string was different from the pattern applying into the SimpleDateFormat contructor
			Log.e("error", ex.toString());
			return null;
		}
	}
	
	@SuppressLint("SimpleDateFormat")
	public String getDateAfter(int k) {
		
		Calendar nowCalendar = Calendar.getInstance();
//		Log.e("dayOfWeek", Integer.toString(dayOfWeek));
		
		long nowTime = nowCalendar.getTimeInMillis();
		long afterTime = nowTime + 86400000 * k;
		
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date afterDate = new Date(afterTime);
		String strDate = df.format(afterDate);
		
		return strDate;	
	}

	public long getUTCTimeInMIilis(long localTimeStamp) {
		int gmtOffset = TimeZone.getDefault().getRawOffset();
		long utcTimeStamp = localTimeStamp - gmtOffset;
		return utcTimeStamp;
	}

	public long getCurrentUTCTimeInMilis() {
		return getUTCTimeInMIilis(getCurrentTimeStamp());
	}

	public String getCurrentUTCTimeAsFormat(String strFormat) {
		return getStringFromTimeStamp(getCurrentUTCTimeInMilis(), strFormat);
	}
}