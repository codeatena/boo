//
//  FeedPhotoTableViewCell.h
//  BOO
//
//  Created by AnCheng on 8/13/15.
//  Copyright (c) 2015 ancheng1114. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FeedPhotoTableViewCell;
@protocol FeedPhotoTableViewCellDelegate <NSObject>

@optional

- (void)playVideo:(FeedPhotoTableViewCell *)cell;

@end

@interface FeedPhotoTableViewCell : UITableViewCell

@property (nonatomic ,assign) IBOutlet UILabel *nameLbl;
@property (nonatomic ,assign) IBOutlet UILabel *dateLbl;

@property (nonatomic ,assign) IBOutlet UILabel *messageLbl;
@property (nonatomic ,assign) IBOutlet UIImageView *photoImageView;
@property (nonatomic ,assign) IBOutlet UIImageView *profileImageView;
@property (nonatomic ,assign) IBOutlet UIButton *playBtn;
@property (nonatomic ,assign) IBOutlet UILabel *countLbl;

- (void)setData:(NSDictionary *)dic;

@property (nonatomic ,weak) id <FeedPhotoTableViewCellDelegate> celldelegate;

@end
