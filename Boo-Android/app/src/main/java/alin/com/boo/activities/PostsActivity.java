package alin.com.boo.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.paging.listview.PagingListView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import alin.com.boo.Consts;
import alin.com.boo.R;
import alin.com.boo.model.Post;
import alin.com.boo.widget.listView.MyListAdapter;
import alin.com.boo.widget.listView.MyListItem;

public class PostsActivity extends ActionBarActivity {

    final String TAG = "PostsActivity";

    PagingListView listView;

    ArrayList<MyListItem> mItems;
    MyListAdapter myListAdapter;
    boolean isFirstPage;

    String nextPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_posts);

        initWidget();
        initValue();
        initEvent();
    }

    private void initWidget() {
        listView = (PagingListView) findViewById(R.id.listView);
        addHeaderView();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initValue() {
        isFirstPage = true;

        mItems = new ArrayList<MyListItem>();
        myListAdapter = new MyListAdapter(this, mItems);
        listView.setAdapter(myListAdapter);

        listView.setHasMoreItems(true);
        listView.setPagingableListener(new PagingListView.Pagingable() {
            @Override
            public void onLoadMoreItems() {
                Log.e(TAG, "onLoadMoreItems");
                if (isFirstPage) {
                    loadPosts();
                } else {
                    loadPostsFromNext();
                }
            }
        });
    }

    private void addHeaderView() {
        View view = View.inflate(this, R.layout.posts_header, null);

        TextView txtUserName = (TextView) view.findViewById(R.id.user_name_textView);
        ImageView imgUserCover = (ImageView) view.findViewById(R.id.user_cover_imageView);
        ImageView imgUserPhoto = (ImageView) view.findViewById(R.id.user_photo_imageView);

        txtUserName.setText(Consts.BOO_NAME);
        Ion.with(imgUserPhoto).load(Consts.BOO_PROFILE_URL);
        Ion.with(imgUserCover).load(Consts.BOO_COVER_URL);

        listView.addHeaderView(view);
    }

    private void initEvent() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e(TAG, position + "");
                Post post = (Post) myListAdapter.getItem(position - 1).data;
                CommentsActivity.post = post;
                startActivity(new Intent(PostsActivity.this, CommentsActivity.class));
            }
        });
    }

    private void loadPosts() {

//        final ProgressDialog progressDialog = DialogUtility.getInstance().getProgressDialog(this);
//        progressDialog.show();

        Log.e(TAG, AccessToken.getCurrentAccessToken().getToken());
        //String boourl = "/Boo?fields=feed.limit(10).fields(type,message,created_time,picture,source,comments.summary(true),likes.summary(true))";
        String url = "Boo";
        Bundle params = new Bundle();
        params.putString("fields", "feed.limit(10).fields(type,message,created_time,picture,source,comments.summary(true),likes.summary(true))");

        GraphRequest request = GraphRequest.newGraphPathRequest(AccessToken.getCurrentAccessToken(), url, new GraphRequest.Callback() {
            @Override
            public void onCompleted(GraphResponse graphResponse) {
                try {
                    JSONObject response =  graphResponse.getJSONObject();
                    Log.e(TAG, response.toString());
                    String id = response.getString("id");
                    JSONObject feed = response.getJSONObject("feed");
                    JSONArray data = feed.getJSONArray("data");

                    for (int i = 0; i < data.length(); i++) {
                        JSONObject jsonPost = data.getJSONObject(i);
                        Post post = new Post(jsonPost);
                        MyListItem item = new MyListItem(R.layout.row_post, post);
                        mItems.add(item);
                    }

                    JSONObject paging = feed.getJSONObject("paging");
                    nextPage = paging.getString("next");

                    isFirstPage = false;
                    listView.onFinishLoading(true, null);
                    myListAdapter.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        request.setParameters(params);
        request.executeAsync();
    }

    private void loadPostsFromNext() {

        Log.e(TAG, "next page: " + nextPage);

        Ion.with(this)
                .load(nextPage)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        try {
                            JSONObject response = new JSONObject(result.toString());

                            Log.e(TAG, response.toString());
                            JSONArray data = response.getJSONArray("data");

                            for (int i = 0; i < data.length(); i++) {
                                JSONObject jsonPost = data.getJSONObject(i);
                                Post post = new Post(jsonPost);
                                MyListItem item = new MyListItem(R.layout.row_post, post);
                                mItems.add(item);
                            }

                            JSONObject paging = response.getJSONObject("paging");
                            nextPage = paging.getString("next");

                            listView.onFinishLoading(true, null);
                            myListAdapter.notifyDataSetChanged();

                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
