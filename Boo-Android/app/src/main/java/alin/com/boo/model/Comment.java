package alin.com.boo.model;

import org.json.JSONObject;

/**
 * Created by User on 8/20/2015.
 */
public class Comment {

    public String id = "";
    public String created_time = "";
    public String message = "";
    public String from_id = "";
    public String from_name = "";

    public Comment(JSONObject jsonObject) {
        try {
            id = jsonObject.getString("id");
            created_time = jsonObject.getString("created_time");
            message = jsonObject.getString("message");
            from_id = jsonObject.getJSONObject("from").getString("id");
            from_name = jsonObject.getJSONObject("from").getString("name");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}