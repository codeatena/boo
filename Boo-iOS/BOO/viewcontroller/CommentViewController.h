//
//  CommentViewController.h
//  BOO
//
//  Created by AnCheng on 8/13/15.
//  Copyright (c) 2015 ancheng1114. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentViewController : UIViewController <UITableViewDataSource ,UITableViewDelegate>

@property (nonatomic ,strong) NSDictionary *postDic;
@property (nonatomic ,strong) NSString *pageUrl;

@property (nonatomic ,strong) NSMutableArray *commmentArr;
@property (nonatomic ,assign) IBOutlet UITableView *commentTableView;

@end
