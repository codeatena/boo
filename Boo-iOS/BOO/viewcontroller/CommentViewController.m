//
//  CommentViewController.m
//  BOO
//
//  Created by AnCheng on 8/13/15.
//  Copyright (c) 2015 ancheng1114. All rights reserved.
//

#import "CommentViewController.h"
#import "FeedPhotoTableViewCell.h"
#import "CommentTableViewCell.h"
#import "UIScrollView+DXRefresh.h"
#import "UIColor+HexString.h"

@interface CommentViewController ()

@end

@implementation CommentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _commentTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [_commentTableView addFooterWithTarget:self action:@selector(refreshFooter) withIndicatorColor:[UIColor colorWithHexString:@"#48A0DC"]];
    
    
    [[[FBSDKGraphRequest alloc] initWithGraphPath:[NSString stringWithFormat:@"/%@/comments" ,_postDic[@"id"]] parameters:nil]
     startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
         
         if (!error) {
             NSLog(@"comments : %@", result);
             _commmentArr = [[NSMutableArray alloc] initWithArray:result[@"data"]];
             [_commentTableView reloadData];
             _pageUrl =  [result[@"paging"] objectForKey:@"next"];
         }
         else
             NSLog(@"error : %@" ,error.localizedDescription);
     }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    self.navigationController.navigationBarHidden = NO;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

    self.navigationController.navigationBarHidden = YES;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableViewDataSource ,UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1 + _commmentArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        FeedPhotoTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"feedCell" forIndexPath:indexPath];
        [cell setData:_postDic];
        cell.playBtn.hidden = YES;
        return cell;
    }
    else
    {
        CommentTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"commentCell" forIndexPath:indexPath];
        NSDictionary *dic = [_commmentArr objectAtIndex:indexPath.row - 1];
        [cell setData:dic];
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
    
}

- (void)refreshFooter
{
    if ([_commentTableView isHeaderRefreshing]) {
        
        [_commentTableView footerEndRefreshing];
        return;
    }
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:_pageUrl parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        [_commmentArr addObjectsFromArray:responseObject[@"data"]];
        _pageUrl =  [responseObject[@"paging"] objectForKey:@"next"];
        [_commentTableView reloadData];
        [_commentTableView footerEndRefreshing];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        
    }];
}

@end
