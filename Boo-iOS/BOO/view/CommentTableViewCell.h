//
//  CommentTableViewCell.h
//  BOO
//
//  Created by AnCheng on 8/14/15.
//  Copyright (c) 2015 ancheng1114. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CommentTableViewCell : UITableViewCell

@property (nonatomic ,assign) IBOutlet UILabel *nameLbl;
@property (nonatomic ,assign) IBOutlet UIImageView *profileImageView;
@property (nonatomic ,assign) IBOutlet UILabel *commentLbl;
@property (nonatomic ,assign) IBOutlet UILabel *dateLbl;

- (void)setData:(NSDictionary *)dic;

@end
