//
//  FeedPhotoTableViewCell.m
//  BOO
//
//  Created by AnCheng on 8/13/15.
//  Copyright (c) 2015 ancheng1114. All rights reserved.
//

#import "FeedPhotoTableViewCell.h"
#import "NSDate+PrettyTimestamp.h"

@implementation FeedPhotoTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setData:(NSDictionary *)dic
{
    NSDate *date = [NewDateFormatter() dateFromString:dic[@"created_time"]];
    _dateLbl.text = [date prettyTimestampSinceNow];
    
    _messageLbl.text = dic[@"message"];
    
    SDWebImageManager *manager = [SDWebImageManager sharedManager];
    [manager downloadImageWithURL:[NSURL URLWithString:BOO_PROFILE_URL] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize){
        
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL){
        
        _profileImageView.image = image;
    }];
    
    [manager downloadImageWithURL:[NSURL URLWithString:dic[@"picture"]] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize){
        
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL){
        
        _photoImageView.image = image;
    }];
    
    if ([dic[@"type"] isEqualToString:@"video"])
    {
        _playBtn.hidden = NO;

    }
    else
        _playBtn.hidden = YES;

    NSString *comments = [[dic[@"comments"] objectForKey:@"summary"] objectForKey:@"total_count"];
    NSString *likes = [[dic[@"likes"] objectForKey:@"summary"] objectForKey:@"total_count"];
    
    _countLbl.text = [NSString stringWithFormat:@" %@ Likes  %@ Comments" ,likes ,comments];
}

static inline NSDateFormatter *NewDateFormatter() {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ssZZZZ"];
    //Optionally for time zone conversions
    //[formatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
    [formatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    
    return formatter;
}

- (IBAction)onPlayVideo:(id)sender
{
    [self.celldelegate playVideo:self];
}

@end
