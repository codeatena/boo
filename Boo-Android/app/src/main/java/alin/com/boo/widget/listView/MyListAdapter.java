package alin.com.boo.widget.listView;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.curioustechizen.ago.RelativeTimeTextView;
import com.koushikdutta.ion.Ion;

import org.w3c.dom.Text;

import java.util.ArrayList;

import alin.com.boo.Consts;
import alin.com.boo.R;
import alin.com.boo.activities.CommentsActivity;
import alin.com.boo.model.Comment;
import alin.com.boo.model.Post;
import alin.lib.utility.TimeUtility;

public class MyListAdapter extends ArrayAdapter<MyListItem> {

    public MyListAdapter(Context context, ArrayList<MyListItem> listItems) {
        super(context, 0, listItems);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view = convertView;
        LayoutInflater inflater = LayoutInflater.from(getContext());

        MyListItem item = getItem(position);

        if (item instanceof MyListItem) {

            if (view == null) {
                view = inflater.inflate(item.layoutID, parent, false);
            }

            Object data = item.data;

            if (data instanceof Post) {
                final Post post = (Post) data;
                ImageView imgUserPhoto = (ImageView) view.findViewById(R.id.post_user_photo_imageView);
                TextView txtPostUserName = (TextView) view.findViewById(R.id.post_username_textView);
                RelativeTimeTextView txtCreatedTime = (RelativeTimeTextView) view.findViewById(R.id.post_create_time_textView);
                TextView txtPostMessage = (TextView) view.findViewById(R.id.post_message_textView);
                ImageView imgPostPhoto = (ImageView) view.findViewById(R.id.post_photo_imageView);
                TextView txtCommentsLikesCount = (TextView) view.findViewById(R.id.post_comments_likes_count_textView);

                txtPostUserName.setText(Consts.BOO_NAME);
                txtCreatedTime.setReferenceTime(TimeUtility.getInstance().getDateFromString(post.created_time, "yyyy-MM-dd'T'HH:mm:ss+SSSS").getTime());
                txtPostMessage.setText(post.message);
                txtCommentsLikesCount.setText(post.likes_count + " Likes " + post.comments_count + " Comments");
                Ion.with(imgPostPhoto).load(post.picture);
                Ion.with(imgUserPhoto).load(Consts.BOO_PROFILE_URL);
            } else if (data instanceof Comment) {
                Comment comment = (Comment) data;
                ImageView imgUserPhoto = (ImageView) view.findViewById(R.id.comment_user_photo_imageView);
                TextView txtCommentUserName = (TextView) view.findViewById(R.id.comment_username_textView);
                TextView txtCommentMessage = (TextView) view.findViewById(R.id.comment_message_textView);
                RelativeTimeTextView txtCommentCreatedTime = (RelativeTimeTextView) view.findViewById(R.id.comment_create_time_textView);

                txtCommentUserName.setText(comment.from_name);
                txtCommentMessage.setText(comment.message);
                txtCommentCreatedTime.setReferenceTime(TimeUtility.getInstance().getDateFromString(comment.created_time, "yyyy-MM-dd'T'HH:mm:ss+SSSS").getTime());
                String urlUserPhoto = "https://graph.facebook.com/" + comment.from_id + "/picture?redirect=true&type=small";
                Ion.with(imgUserPhoto).load(urlUserPhoto);
            }
        }

        return view;
    }
}